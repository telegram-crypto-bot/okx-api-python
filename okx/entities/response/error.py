class Error:
    OK_HTTP_STATUS_CODE = 200
    OK_CODE = "0"
    OK_DEFAULT_CODE = -1
    OK_MESSAGE = ""

    def __init__(
        self,
        http_status_code: int = OK_HTTP_STATUS_CODE,
        code: int = OK_DEFAULT_CODE,
        message: str = OK_MESSAGE,
    ) -> None:
        self._http_status_code = http_status_code
        self._code = int(code)
        self._message = message

    def __str__(self) -> str:
        str_format = (
            f"Error[http_status_code={self._http_status_code}, " f"code={self._code}, " f"message={self._message}]"
        )
        return str_format

    @property
    def http_status_code(self) -> int:
        return self._http_status_code

    @http_status_code.setter
    def http_status_code(self, value: int) -> None:
        self._http_status_code = value

    @property
    def code(self) -> int:
        return self._code

    @code.setter
    def code(self, value: int) -> None:
        self._code = int(value)

    @property
    def message(self) -> str:
        return self._message

    @message.setter
    def message(self, value: str) -> None:
        self._message = value

    def is_ok(self) -> bool:
        is_ok_error = (
            self._http_status_code == Error.OK_HTTP_STATUS_CODE
            and self._code == Error.OK_CODE
            and self._message == Error.OK_MESSAGE
        )
        return is_ok_error
