from typing import Any

from okx.entities.response.error import Error


class Response:
    OK_STATUS = True

    def __init__(self, status: bool = OK_STATUS, data: Any = None, error: Error = Error()) -> None:
        self._status = status
        self._data = data
        self._error = error

    def __str__(self) -> str:
        str_format = f"Response[status={self._status}, " f"data={self._data}, error={self._error}]"
        return str_format

    @property
    def status(self) -> bool:
        return self._status

    @status.setter
    def status(self, value: bool) -> None:
        self._status = value

    @property
    def data(self) -> Any:
        return self._data

    @data.setter
    def data(self, value: Any) -> None:
        self._data = value

    @property
    def error(self) -> Error:
        return self._error

    @error.setter
    def error(self, value: Error) -> None:
        if not value.is_ok():
            self._status = False
        self._error = value
