from enum import Enum


class Symbol(Enum):
    BTC_USDT = "BTC-USDT"


class Side(Enum):
    BUY = "buy"
    SELL = "sell"


class OrderType(Enum):
    MARKET = "market"


class Coin(str, Enum):
    USDT = "USDT"
    BTC = "BTC"


class USDTChain(str, Enum):
    TRC20 = "USDT-TRC20"
    ERC20 = "USDT-ERC20"
