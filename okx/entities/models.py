from decimal import Decimal
from typing import NamedTuple


class EnvVars(NamedTuple):
    base_url: str
    api_key: str
    api_secret: str
    api_passphrase: str


class Balance(NamedTuple):
    coin: str
    total: Decimal
    usdt_total: Decimal
    usdt_price: Decimal


class Order(NamedTuple):
    symbol: str
    order_id: str
    executed_qty: Decimal
    quote_qty: Decimal
    cumulative_quote_qty: Decimal
    avg_price: Decimal
    order_time: int


class Address(NamedTuple):
    chain: str
    address: str
