import base64
import datetime
import hashlib
import hmac
import json
import logging
import urllib.parse
from enum import Enum
from typing import Callable, TypeAlias

import requests
from requests.exceptions import ConnectionError as ReqConnectionError
from requests.exceptions import Timeout, TooManyRedirects

from okx.entities.enums import Coin, OrderType, Side, Symbol
from okx.exceptions import NetworkException

logger = logging.getLogger(__name__)


class _HTTPMethod(Enum):
    GET = "GET"
    POST = "POST"


class _EndpointType(Enum):
    PUBLIC = "Public"
    PRIVATE = "Private"


_PayloadType: TypeAlias = dict[str, str | bytes]

_ParamsType: TypeAlias = dict[str, str | _PayloadType]

ResponseType: TypeAlias = requests.Response


class Connector:
    def __init__(self, base_url: str, api_key: str, api_secret: str, api_passphrase: str) -> None:
        self._base_url = base_url
        self._api_key = api_key
        self._api_secret = api_secret
        self._api_passphrase = api_passphrase

    def _send_request(
        self,
        http_method: _HTTPMethod,
        endpoint_type: _EndpointType,
        url_path: str,
        payload: _PayloadType,
    ) -> ResponseType:
        params = self._prepare_request_params(http_method, url_path, endpoint_type, payload)
        logger.info(
            "Request: http method - %s\nparams: %s.",
            http_method.value,
            params,
        )
        try:
            response = self._dispatch_request(http_method)(**params)
        except (ReqConnectionError, Timeout, TooManyRedirects) as e:
            logger.exception("Network exception.")
            raise NetworkException from e
        return response

    def _get_iso_format_timestamp(self) -> str:
        now = datetime.datetime.utcnow()
        t = now.isoformat("T", "milliseconds")
        return t + "Z"

    def _get_signature(self, timestamp: str, method: _HTTPMethod, request_path: str, payload: _PayloadType) -> bytes:
        request_path = "/api" + request_path

        if method == _HTTPMethod.GET:
            body = "?" + urllib.parse.urlencode(payload)
        else:
            body = json.dumps(payload)

        message = timestamp + method.value + request_path + body
        sign = hmac.new(
            key=self._api_secret.encode(encoding="utf-8"),
            msg=message.encode(encoding="utf-8"),
            digestmod=hashlib.sha256,
        ).digest()
        return base64.b64encode(sign)

    def _prepare_request_params(
        self, http_method: _HTTPMethod, url_path: str, endpoint_type: _EndpointType, payload: _PayloadType
    ) -> _ParamsType:
        url = f"{self._base_url}{url_path}"
        headers = {
            "Content-Type": "application/json",
        }
        params: _ParamsType = {
            "url": url,
        }

        match http_method:
            case _HTTPMethod.GET:
                params["params"] = payload
            case _HTTPMethod.POST:
                params["data"] = json.dumps(payload)

        match endpoint_type:
            case _EndpointType.PUBLIC:
                pass
            case _EndpointType.PRIVATE:
                iso_timestamp = self._get_iso_format_timestamp()

                headers["OK-ACCESS-KEY"] = self._api_key
                headers["OK-ACCESS-SIGN"] = self._get_signature(
                    iso_timestamp, http_method, url_path, payload
                )  # type: ignore
                headers["OK-ACCESS-TIMESTAMP"] = iso_timestamp
                headers["OK-ACCESS-PASSPHRASE"] = self._api_passphrase

        params["headers"] = headers  # type: ignore
        return params

    def _dispatch_request(self, http_method: _HTTPMethod) -> Callable:
        methods: dict[_HTTPMethod, Callable] = {
            _HTTPMethod.GET: requests.get,
            _HTTPMethod.POST: requests.post,
        }
        method = methods[http_method]
        return method

    def get_wallet_balance(self, coin: Coin) -> ResponseType:
        http_method = _HTTPMethod.GET
        endpoint_type = _EndpointType.PRIVATE
        url_path = "/v5/account/balance"
        payload: _PayloadType = {
            "ccy": coin.value,
        }
        response = self._send_request(http_method, endpoint_type, url_path, payload)
        logger.info(
            "Response: status code = %d\nresponse: %s.",
            response.status_code,
            response.text,
        )
        return response

    def place_order(
        self,
        symbol: Symbol,
        qty: str,
        side: Side,
        order_type: OrderType,
    ) -> ResponseType:
        http_method = _HTTPMethod.POST
        endpoint_type = _EndpointType.PRIVATE
        url_path = "/v5/trade/order"
        payload: _PayloadType = {
            "instId": symbol.value,
            "tdMode": "cash",
            "side": side.value,
            "ordType": order_type.value,
            "sz": qty,
        }
        response = self._send_request(http_method, endpoint_type, url_path, payload)
        return response

    def get_spot_order(self, order_id: str, symbol: Symbol) -> ResponseType:
        http_method = _HTTPMethod.GET
        endpoint_type = _EndpointType.PRIVATE
        url_path = "/v5/trade/order"
        payload: _PayloadType = {
            "instId": symbol.value,
            "ordId": order_id,
        }
        response = self._send_request(http_method, endpoint_type, url_path, payload)
        logger.info(
            "Response: status code = %d\nresponse: %s.",
            response.status_code,
            response.text,
        )
        return response

    def get_deposit_address(self, coin: Coin) -> ResponseType:
        http_method = _HTTPMethod.GET
        endpoint_type = _EndpointType.PRIVATE
        url_path = "/v5/asset/deposit-address"
        payload: _PayloadType = {
            "ccy": coin.value,
        }
        response = self._send_request(http_method, endpoint_type, url_path, payload)
        logger.info(
            "Response: status code = %d\nresponse: %s.",
            response.status_code,
            response.text,
        )
        return response

    def get_ticker_price(self, symbol: Symbol) -> ResponseType:
        http_method = _HTTPMethod.GET
        endpoint_type = _EndpointType.PUBLIC
        url_path = "/v5/market/ticker"
        payload: _PayloadType = {
            "instId": symbol.value,
        }
        response = self._send_request(http_method, endpoint_type, url_path, payload)
        logger.info(
            "Response: status code = %d\nresponse: %s.",
            response.status_code,
            response.text,
        )
        return response
