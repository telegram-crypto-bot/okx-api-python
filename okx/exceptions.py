class NetworkException(Exception):
    pass


class NotFoundChainAddress(Exception):
    pass


class NoCoinBalance(Exception):
    pass
