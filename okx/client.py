import logging
from decimal import Decimal
from json import JSONDecodeError

from okx.api_connector import Connector, ResponseType
from okx.entities.enums import Coin, OrderType, Side, Symbol, USDTChain
from okx.entities.models import Address, Balance, Order
from okx.entities.response.error import Error
from okx.entities.response.response import Response
from okx.exceptions import NetworkException, NotFoundChainAddress, NoCoinBalance

logger = logging.getLogger(__name__)


class Client:
    def __init__(self, base_url: str, api_key: str, api_secret: str, api_passphrase: str) -> None:
        self._connector = Connector(base_url, api_key, api_secret, api_passphrase)

    def _check_and_set_error(self, http_response: ResponseType, response: Response) -> None:
        status_code = http_response.status_code

        try:
            response_data = http_response.json()
            response_code = int(response_data["code"])
        except JSONDecodeError:
            logger.exception("JSONDecodeError.")
            response.error = Error(
                http_status_code=Error.OK_HTTP_STATUS_CODE,
                message=http_response.text,
            )
            return

        if "data" in response_data:
            response_data = response_data["data"][0]
        else:
            response.error = Error(
                http_status_code=status_code,
                code=response_code,
                message=response_data.get("msg", http_response.text),
            )
            return
        if status_code == Error.OK_HTTP_STATUS_CODE:
            if response_code == 0:
                return
            else:
                response.error = Error(
                    http_status_code=status_code,
                    code=response_data["sCode"],
                    message=response_data["sMsg"],
                )
        else:
            response.error = Error(
                http_status_code=status_code,
                message=response_data.get("sMsg", http_response.text),
            )

    def _get_network_error(self) -> Error:
        error = Error(
            http_status_code=0,
            message="Network error",
        )
        return error

    def buy_btc(self, usd_count: Decimal) -> Response:
        response = Response()
        symbol = self._get_usdt_pair_symbol(Coin.BTC)
        try:
            resp = self._connector.place_order(
                symbol=symbol,
                qty=str(usd_count),
                side=Side.BUY,
                order_type=OrderType.MARKET,
            )
        except NetworkException:
            logger.exception("Network exception.")
            response.error = self._get_network_error()
            return response
        self._check_and_set_error(http_response=resp, response=response)
        if not response.status:
            return response
        order_id = self._parse_post_order_response(http_response=resp)
        response.data = order_id

        trials = 3
        while trials > 0:
            try:
                resp = self._connector.get_spot_order(order_id, symbol)
                order = self._parse_get_spot_order_response(http_response=resp, quote_qty=usd_count, symbol=symbol)
                response.data = order
            except NetworkException:
                logger.exception("Network exception.")
                response.error = self._get_network_error()
                return response

            self._check_and_set_error(http_response=resp, response=response)
            if response.status:
                return response

            trials -= 1

        return response

    def get_balance(self, coin: Coin) -> Response:
        response = Response()
        try:
            resp = self._connector.get_wallet_balance(coin)
        except NetworkException:
            logger.exception("Network exception.")
            response.error = self._get_network_error()
            return response
        self._check_and_set_error(http_response=resp, response=response)
        if not response.status:
            return response

        try:
            balance = self._parse_get_wallet_balance_response(http_response=resp)
        except NoCoinBalance:
            response.data = Balance(
                coin=coin,
                total=Decimal(0),
                usdt_total=Decimal(0),
                usdt_price=Decimal(0),
            )
            return response

        match coin:
            case Coin.USDT:
                pass
            case _:
                symbol = self._get_usdt_pair_symbol(coin)
                trials = 3
                try:
                    resp_price = self._connector.get_ticker_price(symbol)
                except NetworkException:
                    logger.exception("Network exception.")
                    response.error = self._get_network_error()
                    return response
                self._check_and_set_error(http_response=resp, response=response)
                trials -= 1
                while not response.status and trials > 0:
                    try:
                        resp_price = self._connector.get_ticker_price(symbol)
                    except NetworkException:
                        logger.exception("Network exception.")
                        response.error = self._get_network_error()
                        return response
                    self._check_and_set_error(http_response=resp, response=response)
                    trials -= 1
                if not response.status:
                    return response
                balance = self._parse_get_market_tickers_response(http_response=resp_price, balance=balance)
        response.data = balance
        return response

    def get_btc_price(self) -> Response:
        response = Response()
        symbol = self._get_usdt_pair_symbol(Coin.BTC)
        try:
            resp = self._connector.get_ticker_price(symbol=symbol)
        except NetworkException:
            logger.exception("Network exception.")
            response.error = self._get_network_error()
            return response
        self._check_and_set_error(http_response=resp, response=response)
        if not response.status:
            return response
        price = self._parse_get_ticker_price_response(http_response=resp)
        response.data = price
        return response

    def get_usdt_address(self, network: USDTChain) -> Response:
        response = Response()
        try:
            resp = self._connector.get_deposit_address(coin=Coin.USDT)
        except NetworkException:
            logger.exception("Network exception.")
            response.error = self._get_network_error()
            return response
        self._check_and_set_error(http_response=resp, response=response)
        if not response.status:
            return response
        address = self._parse_get_deposit_address_response(http_response=resp, network=network)
        response.data = address
        return response

    def _parse_post_order_response(self, http_response: ResponseType) -> str:
        return http_response.json()["data"][0]["ordId"]

    def _parse_get_spot_order_response(self, http_response: ResponseType, quote_qty: Decimal, symbol: Symbol) -> Order:
        order_data = http_response.json()["data"][0]
        cumulative_quote_qty = self._calculate_cumulative_quote_qty(
            quote_qty=quote_qty,
            crypto_fee=order_data["fee"],
            crypto_price=order_data["avgPx"],
        )

        return Order(
            symbol=symbol.value,
            order_id=order_data["ordId"],
            executed_qty=Decimal(order_data["accFillSz"]),
            quote_qty=quote_qty,
            cumulative_quote_qty=cumulative_quote_qty,
            avg_price=Decimal(order_data["avgPx"]),
            order_time=int(order_data["cTime"]),
        )

    def _calculate_cumulative_quote_qty(self, quote_qty: Decimal, crypto_fee: str, crypto_price: str) -> Decimal:
        commission_usd = Decimal(crypto_fee) * Decimal(crypto_price)
        return quote_qty + commission_usd

    def _parse_get_ticker_price_response(self, http_response: ResponseType) -> Decimal:
        price_data = http_response.json()["data"][0]["last"]
        return Decimal(price_data)

    def _parse_get_deposit_address_response(self, http_response: ResponseType, network: USDTChain) -> Address:
        addresses = http_response.json()["data"]
        address = next(
            (address["addr"] for address in addresses if address["chain"] == network.value),
            None,
        )
        if address is None:
            raise NotFoundChainAddress
        return Address(
            chain=network.value,
            address=address,
        )

    def _get_usdt_pair_symbol(self, coin: Coin) -> Symbol:
        match coin:
            case Coin.BTC:
                symbol = Symbol.BTC_USDT
            case _:
                symbol = Symbol.BTC_USDT
        return symbol

    def _parse_get_market_tickers_response(self, http_response: ResponseType, balance: Balance) -> Balance:
        price_data = http_response.json()["data"][0]["last"]
        return Balance(
            coin=balance.coin,
            total=balance.total,
            usdt_total=balance.total * Decimal(price_data),
            usdt_price=Decimal(price_data),
        )

    def _parse_get_wallet_balance_response(self, http_response: ResponseType) -> Balance:
        balance_data = http_response.json()["data"][0]["details"]

        if not len(balance_data):
            raise NoCoinBalance

        balance_data = balance_data[0]
        coin = Coin(balance_data["ccy"])
        match coin:
            case Coin.USDT:
                balance = Balance(
                    coin=coin,
                    total=Decimal(balance_data["availBal"]),
                    usdt_total=Decimal(balance_data["availBal"]),
                    usdt_price=Decimal(1),
                )
            case _:
                balance = Balance(
                    coin=coin,
                    total=Decimal(balance_data["availBal"]),
                    usdt_total=Decimal(-1),
                    usdt_price=Decimal(-1),
                )
        return balance
