# OKX API Connector for Python

## Create the virtual environment

To create the virtual environment:

```bash
./create-venv
```

## Start

```bash
./start
```
