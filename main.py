import logging
import os
import sys
from decimal import Decimal
from pathlib import Path

from dotenv import load_dotenv

from okx.api_connector import Connector
from okx.client import Client
from okx.entities.models import EnvVars

logger = logging.getLogger(__name__)


def set_env_vars() -> None:
    dotenv_path = Path(".env")
    if not dotenv_path.is_file():
        logger.error("Wrong .env-file path.")
        sys.exit(1)
    load_dotenv(dotenv_path)


def do_requests(env_vars: EnvVars) -> None:
    kwargs = {
        "base_url": env_vars.base_url,
        "api_key": env_vars.api_key,
        "api_secret": env_vars.api_secret,
        "api_passphrase": env_vars.api_passphrase,
    }
    connector = Connector(**kwargs)
    client = Client(**kwargs)

    print(client.buy_btc(Decimal("0.00")))


def get_env_vars() -> EnvVars:
    base_url = os.getenv("BASE_URL")
    if base_url is None:
        logger.error("BASE_URL is not provided.")
        sys.exit(1)
    api_key = os.getenv("API_KEY")
    if api_key is None:
        logger.error("API_KEY is not provided.")
        sys.exit(1)
    api_secret = os.getenv("API_SECRET")
    if api_secret is None:
        logger.error("API_SECRET is not provided.")
        sys.exit(1)
    api_passphrase = os.getenv("API_PASSPHRASE")
    if api_passphrase is None:
        logger.error("API_PASSPHRASE is not provided.")
        sys.exit(1)
    env_vars = EnvVars(base_url, api_key, api_secret, api_passphrase)
    return env_vars


def main() -> None:
    logging.basicConfig(level=logging.INFO)

    set_env_vars()
    env_vars = get_env_vars()

    do_requests(env_vars)


if __name__ == "__main__":
    main()
